#!/usr/bin/env bash
unamestr=`uname`

timeOut=$1

# If this is mac
if [[ "$unamestr" == "Darwin" ]]; then
    folder="macos"
else
    folder="linux"
fi

if [ "$1" = "" ]
then
timeOut=60
fi

echo $timeOut

binpath=$(pwd)/bin/$folder
echo "Bin Path: $binpath"

export PATH=$binpath:$binpath/firefox:$PATH

# Log level
LOG_LEVEL="WARNING"
# LOG_LEVEL="FINEST"
# LOG_LEVEL="WARNING"

cmd="java -Dselenium.LOGGER.level=$LOG_LEVEL -Dselenium.LOGGER=log/selenium.log -Dwebdriver.chrome.verboseLogging=true -jar selenium-server-standalone-3.14.0.jar -port 44440 -browserTimeout $timeOut -timeout $timeOut"
echo $cmd

$($cmd)
