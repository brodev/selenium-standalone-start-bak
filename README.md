Start Selenium Server
#####################

## Standalone

### Prerequiste

Run following commands:

- `sh download_firefox.sh` and `sh download_chrome.sh`
- `sudo apt install xvfb openjdk-8-jre libdbus-glib-1-2 -y`


### Crontab 

```bash
# Selenium server
@reboot cd /home/beeketing/selenium && bash start.sh
*/10 * * * * cd /home/beeketing/selenium && bash start.sh
*/2 * * * * cd /home/beeketing/selenium && bash stop_hang_browsers.sh
```

## Run as a GRID

```bash
# Start grid
bash grid_start_hub.sh

# Start a node
bash grid_start_node.sh http://selenium-host:port/grid/register localPort maxSession
# bash grid_start_node.sh http://selenium.beeketing.com:44440/grid/register 5556 20
```

### crontab

```bash
# Selenium Hub server
@reboot cd /home/beeketing/selenium && grid_start_hub.sh

# Selenium Node server
@reboot cd /home/beeketing/selenium && grid_start_node.sh http://selenium.beeketing.com:44440/grid/register 5556 30
```
