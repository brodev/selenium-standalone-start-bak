#!/usr/bin/env bash

kill $(ps -eo comm,pid,etimes | grep firefox | awk '{if ($3 > 800) { print $2}}') > /tmp/null
kill $(ps -eo comm,pid,etimes | grep chrome | awk '{if ($3 > 800) { print $2}}') > /tmp/null
kill $(ps -eo comm,pid,etimes | grep geckodriver | awk '{if ($3 > 800) { print $2}}') > /tmp/null
