#!/usr/bin/env bash
unamestr=`uname`
# If this is mac
if [[ "$unamestr" == "Darwin" ]]; then
    folder="macos"
else
    folder="linux"
fi

binpath=$(pwd)/bin/$folder
echo "Bin Path: $binpath"

export PATH=$binpath:$binpath/firefox:$PATH

# Log level
LOG_LEVEL="WARNING"
# LOG_LEVEL="FINEST"
# LOG_LEVEL="WARNING"

timeOut=$1

if [ "$1" = "" ]
then
timeOut=60
fi

echo "time out: $timeOut"

cmd="java -Dselenium.LOGGER.level=$LOG_LEVEL -Dselenium.LOGGER=log/selenium.log -Dwebdriver.chrome.verboseLogging=true -jar selenium-server-standalone-3.14.0.jar -port 44440 -browserTimeout $timeOut -timeout $timeOut"
echo $cmd
# If this is mac
if [[ "$unamestr" == "Darwin" ]]; then
    echo "Starting for MacOS"
    $($cmd)
else
    echo "Starting for Linux"
    nohup xvfb-run -e /dev/stdout --server-args="-screen 0 1400x1050x24" $cmd > nohup.out 2>&1 &
fi
