#!/usr/bin/env bash
unamestr=`uname`

# If this is mac
if [[ "$unamestr" == "Darwin" ]]; then
    folder="macos"
else
    folder="linux"
fi

binpath=$(pwd)/bin/$folder
echo "Bin Path: $binpath"

# Log level
LOG_LEVEL="ALL"
# LOG_LEVEL="FINEST"
# LOG_LEVEL="WARNING"

HUB=$1
PORT=$2
MAXSESSION=$3

echo "Hub: $HUB"
echo "Port: $PORT"
echo "MaxSession: $MAXSESSION"

cmd="java -Dselenium.LOGGER.level=$LOG_LEVEL -Dselenium.LOGGER=log/node.log -jar selenium-server-standalone-3.14.0.jar -role node -nodeConfig grid_config/node.json -hub $HUB -port $PORT -maxSession $MAXSESSION"
echo "Running command:"
echo $cmd

# If this is mac
if [[ "$unamestr" == "Darwin" ]]; then
    echo "Starting for MacOS"
    $($cmd)
else
    echo "Starting for Linux"
    nohup xvfb-run --server-args="-screen 0 1400x1050x24" $cmd > nohup.out 2>&1 &
fi