#!/usr/bin/env bash
unamestr=`uname`

# Log level
LOG_LEVEL="ALL"
# LOG_LEVEL="FINEST"
# LOG_LEVEL="WARNING"

cmd="java -Dselenium.LOGGER.level=$LOG_LEVEL -Dselenium.LOGGER=log/grid.log -jar selenium-server-standalone-3.14.0.jar -role hub -hubConfig grid_config/hub.json"
echo $cmd

# If this is mac
if [[ "$unamestr" == "Darwin" ]]; then
    echo "Starting for MacOS"
    $($cmd)
else
    echo "Starting for Linux"
    nohup $cmd > nohup.out 2>&1 &
fi