#!/usr/bin/env bash

# Kill all xvfb & selenium server
kill $(ps aux | grep selenium-server-standalone-3.14.0.jar | awk '{print $2 }') > /tmp/null
kill $(ps aux | grep Xvfb | awk '{print $2 }') > /tmp/null
kill $(ps aux | grep geckodriver | awk '{print $2 }') > /tmp/null
kill $(ps aux | grep firefox | awk '{print $2 }') > /tmp/null
kill $(ps aux | grep chrome | awk '{print $2 }') > /tmp/null

echo "Done"