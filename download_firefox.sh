#!/usr/bin/env bash
wget http://ftp.mozilla.org/pub/firefox/releases/62.0/linux-$(uname -m)/en-US/firefox-62.0.tar.bz2
tar -xjf firefox-62.0.tar.bz2
mv firefox bin/linux